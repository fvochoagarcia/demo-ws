package com.reto.api1.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import com.reto.api1.entity.TipoCambio;

@Repository
public interface TipoCambioRepository extends CrudRepository<TipoCambio, Long>{
	
	@Query("select tc from TipoCambio tc where tc.monedaOrigen=?1 and tc.monedaDestino=?2")
	public Optional<TipoCambio> findByMonedaOrigenByMonedaDestino(String monedaOrigen, String monedaDestino);
	
}
