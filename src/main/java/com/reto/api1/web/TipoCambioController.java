package com.reto.api1.web;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.reto.api1.service.TipoCambioService;
import com.reto.api1.service.dto.request.TipoCambioRequest;
import com.reto.api1.service.dto.response.TipoCambioResponse;
import com.reto.api1.web.dto.request.RequestWebCotizacion;
import com.reto.api1.web.dto.request.RequestWebTipoCambio;
import com.reto.api1.web.dto.response.ResponseWebCotizacion;
import com.reto.api1.web.dto.response.ResponseWebTipoCambio;

import io.reactivex.Single;

@RestController
public class TipoCambioController {
	
	@Autowired
	TipoCambioService tipoCambioService;
	
//	@PostMapping("/cotizacion")
//	public ResponseCotizacion tipoCambio(@RequestBody RequestCotizacion request)
//	{
//		ResponseCotizacion response = new ResponseCotizacion();
//		Optional<TipoCambio> tipoCambio = tipoCambioRepository.findByMonedaOrigenByMonedaDestino(request.getMonedaOrigen().toUpperCase(), request.getMonedaDestino().toUpperCase());
//		if(tipoCambio.isPresent()) {
//			TipoCambio tc = tipoCambio.get();
//			if(!StringUtils.pathEquals(request.getMonedaOrigen(), request.getMonedaDestino())) {
//				BigDecimal montoTipoCambio = tc.getCambio().multiply(request.getMonto());
//				response.setMontoTipoCambio(montoTipoCambio);
//				
//			}else {
//				response.setMontoTipoCambio(request.getMonto());
//			}
//			
//			String id = String.valueOf(new Random().nextInt());
//			response.setIdResponseTipoCambio(id);
//			response.setMonto(request.getMonto());
//			response.setMonedaOrigen(tc.getMonedaOrigen());
//			response.setMonedaDestino(tc.getMonedaDestino());
//			response.setTipoCambio(tc.getCambio());	
//		}
//		
//		return response;
//	
//	}
	
//	@PostMapping("/cotizacionRX")
////	Chekar si tiene que usarse el ResponseEntity - public Single<ResponseEntity<ResponseCotizacion>> tipoCambioRX(@RequestBody RequestCotizacion request)
//	public Single<ResponseCotizacion> tipoCambioRX(@RequestBody RequestCotizacion request)
//	{
//		
//		return Single.create(singleSubscriber -> {
//		ResponseCotizacion response = new ResponseCotizacion();
//		Optional<TipoCambio> tipoCambio = tipoCambioRepository.findByMonedaOrigenByMonedaDestino(request.getMonedaOrigen().toUpperCase(), request.getMonedaDestino().toUpperCase());
//		if(tipoCambio.isPresent()) {
//			TipoCambio tc = tipoCambio.get();
//			if(!StringUtils.pathEquals(request.getMonedaOrigen(), request.getMonedaDestino())) {
//				BigDecimal montoTipoCambio = tc.getCambio().multiply(request.getMonto());
//				response.setMontoTipoCambio(montoTipoCambio);
//				
//			}else {
//				response.setMontoTipoCambio(request.getMonto());
//			}
//			
//			String id = String.valueOf(new Random().nextInt());
//			response.setIdResponseTipoCambio(id);
//			response.setMonto(request.getMonto());
//			response.setMonedaOrigen(tc.getMonedaOrigen());
//			response.setMonedaDestino(tc.getMonedaDestino());
//			response.setTipoCambio(tc.getCambio());	
//			singleSubscriber.onSuccess(response);
//		}else {
//			singleSubscriber.onError(new EntityNotFoundException());
//		}
//	});
//	
//	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("/cotizacionRX")
//	Chekar si tiene que usarse el ResponseEntity - public Single<ResponseEntity<ResponseCotizacion>> tipoCambioRX(@RequestBody RequestCotizacion request)
	public Single<ResponseWebCotizacion> tipoCambioRX(@RequestBody RequestWebCotizacion requestWebCotizacion)
	{
		
		return Single.create(singleSubscriber -> {
		ResponseWebCotizacion responseWebCotizacion= new ResponseWebCotizacion();
		Single<TipoCambioResponse> tipoCambioResponse = tipoCambioService.findByMonedaOrigenByMonedaDestino(requestWebCotizacion.getMonedaOrigen().toUpperCase(), requestWebCotizacion.getMonedaDestino().toUpperCase());
		if(!ObjectUtils.isEmpty(tipoCambioResponse)) {

			TipoCambioResponse tc = new TipoCambioResponse();
			BeanUtils.copyProperties(tipoCambioResponse.blockingGet(), tc);
			if(!StringUtils.pathEquals(requestWebCotizacion.getMonedaOrigen(), requestWebCotizacion.getMonedaDestino())) {
				BigDecimal montoTipoCambio = tc.getCambio().multiply(requestWebCotizacion.getMonto());
				responseWebCotizacion.setMontoTipoCambio(montoTipoCambio);
				
			}else {
				responseWebCotizacion.setMontoTipoCambio(requestWebCotizacion.getMonto());
			}
			
			String id = String.valueOf(new Random().nextInt());
			responseWebCotizacion.setIdResponseTipoCambio(id);
			responseWebCotizacion.setMonto(requestWebCotizacion.getMonto());
			responseWebCotizacion.setMonedaOrigen(tc.getMonedaOrigen());
			responseWebCotizacion.setMonedaDestino(tc.getMonedaDestino());
			responseWebCotizacion.setTipoCambio(tc.getCambio());	
			singleSubscriber.onSuccess(responseWebCotizacion);
		}else {
			singleSubscriber.onError(new EntityNotFoundException());
		}
	});
	
	}
	
//	@PostMapping("/actualizarTipoCambio/{id}")
//	public Optional<TipoCambio> actualizarTipoCambio(@RequestBody RequestTipoCambio requestTipoCambio, @PathVariable Long id)
//	{
//		Optional<TipoCambio> tipoCambio = tipoCambioRepository.findById(id);
//		if (tipoCambio.isPresent()) {
//			TipoCambio tc = tipoCambio.get();
//			tc.setCambio(requestTipoCambio.getCambio());
//			tipoCambioRepository.save(tc);
//		}
//		return tipoCambio;
//		
//	}
	
//	@PostMapping("/actualizarTipoCambioRX/{id}")
//	public Single<TipoCambio> actualizarTipoCambioRX(@RequestBody RequestTipoCambio requestTipoCambio, @PathVariable Long id)
//	{
//		return Single.create(singleSubscriber -> {
//		Optional<TipoCambio> tipoCambio = tipoCambioRepository.findById(id);
//		
//		if (tipoCambio.isPresent()) {
//			TipoCambio tc = tipoCambio.get();
//			tc.setCambio(requestTipoCambio.getCambio());
//			tipoCambioRepository.save(tc);
//			singleSubscriber.onSuccess(tc);
//		}else {
//			singleSubscriber.onError(new EntityNotFoundException());
//		}
//		});
//	}
	

//	@PostMapping("/actualizarTipoCambioRX/{id}")
//	public Single<TipoCambio> actualizarTipoCambioRX(@RequestBody RequestTipoCambio requestTipoCambio, @PathVariable Long id)
//	{
//		return Single.create(singleSubscriber -> {
//		Optional<TipoCambio> tipoCambio = tipoCambioRepository.findById(id);
//		
//		if (tipoCambio.isPresent()) {
//			TipoCambio tc = tipoCambio.get();
//			tc.setCambio(requestTipoCambio.getCambio());
//			tipoCambioRepository.save(tc);
//			singleSubscriber.onSuccess(tc);
//		}else {
//			singleSubscriber.onError(new EntityNotFoundException());
//		}
//		});
//	}
	
	@PostMapping("/actualizarTipoCambioRX/{id}")
	public Single<ResponseWebTipoCambio> actualizarTipoCambioRX(@RequestBody RequestWebTipoCambio requestWebTipoCambio, @PathVariable Long id)
	{
		return Single.create(singleSubscriber -> {
			Single<TipoCambioResponse> tipoCambioResponse = tipoCambioService.findById(id);
			if(!ObjectUtils.isEmpty(tipoCambioResponse)) {
				
				TipoCambioRequest tipoCambioRequest = new TipoCambioRequest();
        		BeanUtils.copyProperties(tipoCambioResponse.blockingGet(), tipoCambioRequest);
        		tipoCambioRequest.setCambio(requestWebTipoCambio.getCambio());
        		Single<TipoCambioResponse> tipoCambioResponseSave = tipoCambioService.save(tipoCambioRequest);
        		ResponseWebTipoCambio responseWebTipoCambio = new ResponseWebTipoCambio();
        		BeanUtils.copyProperties(tipoCambioResponseSave.blockingGet(), responseWebTipoCambio);
				singleSubscriber.onSuccess(responseWebTipoCambio);
			}else {
				singleSubscriber.onError(new EntityNotFoundException());
			}
		});
	}
	
//	@GetMapping("/listTipoCambio")
//	public List<TipoCambio> obtenerTipoCambio()
//	{
//		Iterable<TipoCambio> result = tipoCambioRepository.findAll();
//		List<TipoCambio> tipoCambioList = new ArrayList<TipoCambio>();
//		result.forEach(tipoCambioList::add);
//		return tipoCambioList;
//	}
	
//	@GetMapping("/listTipoCambioRX")
//	public Single<List<TipoCambio>> obtenerTipoCambioRX() {
//				
//        return Single.create(singleSubscriber -> {
//        	Iterable<TipoCambio> result = tipoCambioRepository.findAll();
//        	if(ObjectUtils.isEmpty(result)) {
//        	List<TipoCambio> tipoCambioList = new ArrayList<TipoCambio>();
//        	result.forEach(tipoCambioList::add);
//            singleSubscriber.onSuccess(tipoCambioList);
//        	}else {
//        		singleSubscriber.onError(new EntityNotFoundException());
//        	}
//        });
//    }


	@GetMapping("/listTipoCambioRX")
	public Single<List<ResponseWebTipoCambio>> obtenerTipoCambioRX() {
				
        return Single.create(singleSubscriber -> {
        	Single<List<TipoCambioResponse>>  result = tipoCambioService.findAll();
        	if(!ObjectUtils.isEmpty(result)) {
	        	List<ResponseWebTipoCambio> tipoCambioList = new ArrayList<ResponseWebTipoCambio>();
	        	List<TipoCambioResponse> valor  =result.blockingGet();
	        	valor.forEach(item -> {
                	ResponseWebTipoCambio data = new ResponseWebTipoCambio();
                    BeanUtils.copyProperties(item, data);
                    tipoCambioList.add(data);
	        	});
	            singleSubscriber.onSuccess(tipoCambioList);
        	}else {
        		singleSubscriber.onError(new  EntityNotFoundException());
        	}
        });
    }	
}










