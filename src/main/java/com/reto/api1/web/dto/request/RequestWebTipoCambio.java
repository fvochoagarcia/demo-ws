package com.reto.api1.web.dto.request;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestWebTipoCambio {
	
	@JsonProperty
	private Long idRequestTipoCambio;

	@JsonProperty
	private BigDecimal cambio;

	@JsonProperty
	private String moneda;

	public Long getIdRequestTipoCambio() {
		return idRequestTipoCambio;
	}

	public void setIdRequestTipoCambio(Long idRequestTipoCambio) {
		this.idRequestTipoCambio = idRequestTipoCambio;
	}

	public BigDecimal getCambio() {
		return cambio;
	}

	public void setCambio(BigDecimal cambio) {
		this.cambio = cambio;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}	

}
