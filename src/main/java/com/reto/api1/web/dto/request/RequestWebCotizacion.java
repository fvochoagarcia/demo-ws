package com.reto.api1.web.dto.request;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestWebCotizacion {
	
	@JsonProperty
	private String idRequestTipoCambio;

	@JsonProperty
	private BigDecimal monto;

	@JsonProperty
	private String monedaOrigen;

	@JsonProperty
	private String monedaDestino;

	public String getIdRequestTipoCambio() {
		return idRequestTipoCambio;
	}

	public void setIdRequestTipoCambio(String idRequestTipoCambio) {
		this.idRequestTipoCambio = idRequestTipoCambio;
	}
	
	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public String getMonedaOrigen() {
		return monedaOrigen;
	}

	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}

	public String getMonedaDestino() {
		return monedaDestino;
	}

	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}
	
}
