package com.reto.api1.web.dto.response;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseWebCotizacion {
	
	@JsonProperty
	private String idResponseTipoCambio;

	@JsonProperty
	private BigDecimal monto;

	@JsonProperty
	private String monedaOrigen;

	@JsonProperty
	private String monedaDestino;
	
	@JsonProperty
	private BigDecimal montoTipoCambio;
	
	@JsonProperty
	private BigDecimal tipoCambio;

	public String getIdResponseTipoCambio() {
		return idResponseTipoCambio;
	}

	public void setIdResponseTipoCambio(String idResponseTipoCambio) {
		this.idResponseTipoCambio = idResponseTipoCambio;
	}

	public BigDecimal getMonto() {
		return monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public String getMonedaOrigen() {
		return monedaOrigen;
	}

	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}

	public String getMonedaDestino() {
		return monedaDestino;
	}

	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}

	public BigDecimal getMontoTipoCambio() {
		return montoTipoCambio;
	}

	public void setMontoTipoCambio(BigDecimal montoTipoCambio) {
		this.montoTipoCambio = montoTipoCambio;
	}

	public BigDecimal getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(BigDecimal tipoCambio) {
		this.tipoCambio = tipoCambio;
	}

}
