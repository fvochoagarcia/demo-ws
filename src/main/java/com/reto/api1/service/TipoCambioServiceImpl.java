package com.reto.api1.service;

import io.reactivex.Single;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.reto.api1.entity.TipoCambio;
import com.reto.api1.repository.TipoCambioRepository;
import com.reto.api1.service.dto.request.TipoCambioRequest;
import com.reto.api1.service.dto.response.TipoCambioResponse;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

@Service
public class TipoCambioServiceImpl implements TipoCambioService {

    @Autowired
    private TipoCambioRepository tipoCambioRepository;


	@Override
	public Single<TipoCambioResponse> findByMonedaOrigenByMonedaDestino(String monedaOrigen,
			String monedaDestino) {
		return Single.create(singleSubscriber -> {
			Optional<TipoCambio> tipoCambioOptional = tipoCambioRepository.findByMonedaOrigenByMonedaDestino(monedaOrigen, monedaDestino);
			if(tipoCambioOptional.isPresent()) {	
				TipoCambioResponse tipoCambioResponse= new TipoCambioResponse();
				BeanUtils.copyProperties(tipoCambioOptional.get(), tipoCambioResponse);
				singleSubscriber.onSuccess(tipoCambioResponse);
			}else {
				singleSubscriber.onError(new EntityNotFoundException());
			}
		});
	}
	
	@Override
    public Single<List<TipoCambioResponse>> findAll() {
		 return findAllTipoCambioInRepository()
	                .map(this::toTipoCambioResponseList);
    }
	
	private Single<List<TipoCambio>> findAllTipoCambioInRepository() {
        return Single.create(singleSubscriber -> {
            List<TipoCambio> tipoCambio = (List<TipoCambio>) tipoCambioRepository.findAll();         
            singleSubscriber.onSuccess(tipoCambio);
        });
    }

    private List<TipoCambioResponse> toTipoCambioResponseList(List<TipoCambio> tipoCambioList) {
        return tipoCambioList
                .stream()
                .map(this::toTipoCambioResponse)
                .collect(Collectors.toList());
    }
    
    private TipoCambioResponse toTipoCambioResponse(TipoCambio tipoCambio) {
    	TipoCambioResponse tipoCambioResponse = new TipoCambioResponse();
        BeanUtils.copyProperties(tipoCambio, tipoCambioResponse);
        return tipoCambioResponse;
    }

	@Override
	public Single<TipoCambioResponse> findById(Long id) {
		return Single.create(singleSubscriber -> {
			Optional<TipoCambio> tipoCambioOptional = tipoCambioRepository.findById(id);
			if(tipoCambioOptional.isPresent()) {	
				TipoCambioResponse tipoCambioResponse= new TipoCambioResponse();
				BeanUtils.copyProperties(tipoCambioOptional.get(), tipoCambioResponse);
				singleSubscriber.onSuccess(tipoCambioResponse);
			}else {
				singleSubscriber.onError(new EntityNotFoundException());
			}
		});
	}

	@Override
	public Single<TipoCambioResponse> save(TipoCambioRequest tipoCambioRequest) {
		return Single.create(singleSubscriber -> {
			TipoCambio tipoCambio = new TipoCambio();
			BeanUtils.copyProperties(tipoCambioRequest, tipoCambio);
			TipoCambio tipoCambioSave = tipoCambioRepository.save(tipoCambio);
			if(!ObjectUtils.isEmpty(tipoCambioSave)) {	
				TipoCambioResponse tipoCambioResponse= new TipoCambioResponse();
				BeanUtils.copyProperties(tipoCambioSave, tipoCambioResponse);
				singleSubscriber.onSuccess(tipoCambioResponse);
			}else {
				singleSubscriber.onError(new EntityNotFoundException());
			}
		});
	}
	
}


