package com.reto.api1.service;

import io.reactivex.Single;

import java.util.List;

import com.reto.api1.service.dto.request.TipoCambioRequest;
import com.reto.api1.service.dto.response.TipoCambioResponse;

public interface TipoCambioService {

    Single<TipoCambioResponse> findByMonedaOrigenByMonedaDestino(String monedaOrigen, String monedaDestino);
    
    Single<List<TipoCambioResponse>> findAll();
    
    Single<TipoCambioResponse> findById(Long id);
    
    Single<TipoCambioResponse> save(TipoCambioRequest tipoCambioRequest);

}
